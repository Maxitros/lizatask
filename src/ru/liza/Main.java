package ru.liza;

public class Main {
	public static void main(String args[])
	{
		Car[] cars = new Car[1];
		cars[0] = new Car(CarTypes.COUPE, Colors.Red, EngineType.EngV2, "Lada12", Producers.Lada, "A123BC");
		for(Car car:cars)
		{
			if(car.ProdName==Producers.Lada&&car.color==Colors.Red)
			{
				car.PrintSpec();
			}
		}
		
	}
	

}
