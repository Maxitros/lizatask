package ru.liza;

import java.util.Random;

public class Car extends Modification
{
	public Colors color;
	public String vin;
	public Car(CarTypes cartype,Colors color,EngineType engine,String model,Producers producer,String vin )
	{
		this.cartype=cartype;
		this.color = color;
		this.engine=engine;
		this.ModName=model;
		this.ProdName=producer;
		this.vin=vin;
		
	}
	
	public Car()
	{
		
		
	}

	public void PrintSpec() {
		System.out.print(this.ProdName+" "+this.ModName+" "+ this.cartype+" "
				+ this.engine+" "+" "+this.color+ " "+ this.vin);
		
	}
}
